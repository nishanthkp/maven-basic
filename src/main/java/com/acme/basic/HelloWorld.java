package com.acme.basic;

public class HelloWorld {

  void sayHello() {
    System.out.println("Hello World from gitlab with code definition");
  }

  void notCovered() {
    System.out.println("This method is not covered by unit tests");
  }

}
